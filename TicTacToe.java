import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

		printBoard(board);

		int counter = 0;

		while (true) {
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();

			while (row < 1 || row > 3 || col < 1 || col > 3 || board[row - 1][col - 1] != ' ') {
				System.out.println("Invalid number/numbers.");
				System.out.print("Player 1 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 1 enter col number:");
				col = reader.nextInt();
			}

			board[row - 1][col - 1] = 'X';
			printBoard(board);

			counter++;

			if (check(board)) {
				System.out.println("Player 1 wins.");
				break;
			}

			if (counter >= 9) {
				System.out.print("Draw !");
				reader.close();
				break;
			}

			System.out.print("Player 2 enter row number:");
			row = reader.nextInt();
			System.out.print("Player 2 enter column number:");
			col = reader.nextInt();

			while (row < 1 || row > 3 || col < 1 || col > 3 || board[row - 1][col - 1] != ' ') {
				System.out.println("Invalid number/numbers.");
				System.out.print("Player 2 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				col = reader.nextInt();
			}

			board[row - 1][col - 1] = 'O';
			printBoard(board);

			counter++;

			if (check(board)) {
				System.out.println("Player 2 wins.");
				break;
			}

		}

	}



	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}
	public static boolean check(char[][] board) {
		if (board[0][0] != ' ' && board[0][0] == board[1][1] && board[0][0] == board[2][2])
			return true;
		else if ( board[0][2] != ' ' && board[0][2] == board[1][1] && board[0][2] == board[2][0])
			return true;
		for (int i=0; i<3; i++) {
			if (board[i][0] != ' ' && board[i][0] == board[i][1] && board[i][0] == board[i][2])
				return true;
			else if (board[0][i] != ' ' && board[0][i] == board[1][i] && board[0][i] == board[2][i])
				return true;
		}
		return false;
	}
}